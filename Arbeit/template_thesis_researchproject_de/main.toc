\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {ngerman}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Einleitung}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Zielsetzung}{1}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Vorgehensweise und Aufbau der Arbeit}{2}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Grundlagen}{3}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Analyse}{4}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}NeuroRace Projekt}{4}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Bisherige Entscheidungsfindung}{5}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Konzeption}{6}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Implementierung}{6}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Recurrent Neural Network}{7}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Implementierung}{11}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Anpassungen der YAML-Konfigurationsdatei}{11}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Quellcodeanpassungen neuroracer-ai}{12}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Quellcodeanpassungen neuroracer-ai-sl}{13}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Recurrent Neural Network}{14}{section.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Test}{16}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Testsystem}{16}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.2}Fahrtests}{16}{section.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {7}Evaluation}{18}{chapter.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.1}Daten}{18}{section.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.2}Parameteranzahl}{19}{section.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.3}Sequenzl\IeC {\"a}nge}{19}{section.7.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.4}CNN Kernel}{21}{section.7.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.5}Augmentierung}{21}{section.7.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7.6}RNN vs. CNN}{22}{section.7.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {8}Fazit}{23}{chapter.8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.1}Zusammenfassung}{23}{section.8.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.2}Kritischer R\IeC {\"u}ckblick}{23}{section.8.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8.3}Ausblick}{24}{section.8.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Abbildungsverzeichnis}{26}{chapter*.15}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Tabellenverzeichnis}{27}{chapter*.16}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Source Code Content}{28}{chapter*.17}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Literaturverzeichnis}{29}{chapter*.18}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Onlinereferenzen}{30}{chapter*.19}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Anhang}{31}{chapter*.19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}Gazebo Teststrecke}{31}{section.A.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Eigenst\IeC {\"a}ndigkeitserkl\IeC {\"a}rung}{32}{chapter*.22}
