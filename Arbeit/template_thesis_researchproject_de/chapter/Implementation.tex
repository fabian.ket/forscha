\chapter{Implementierung}
\label{cha:implmentation}


\section{Anpassungen der YAML-Konfigurationsdatei}
Damit die neuen RNN-Implementierungen beim Training oder in der Simulation verwendet werden, muss der neue Eintrag $seqence\_size$ in der jeweiligen YAML-Konfigurationsdatei gesetzt werden. Wenn $sequence\_size$ in der Backend-Konfiguration fehlt oder dieser, wie in der folgenden Abbildung auf null oder bei Python auf None gesetzt wird, wird die alte Implementierung verwendet.

\begin{lstlisting}[caption={YAML-Konfigurationsdatei mit neuen Backend-Parameter}, captionpos=b, label={lst:yaml-backend-config}]
# Backend parameter for the used backend and learning type
backend_config:
  backend: keras # currently only keras can be used
  learning_type: supervised_learning	# in the supervised learning project
                                    	   # supervised_learning should be used
  sequence_size: !!null	# if set, RNN backend is used
                       	  # set to the length of the wanted single sequence length
\end{lstlisting}

Sollte jedoch der Eintrag in der Konfigurationsdatei vorhanden sein und auf z.B. 10 gesetzt sein, so wird die neue RNN-Implementierung verwendet. Der an dieser Stelle angegebene Wert bestimmt, wie lang eine einzelne Sequenz für das verwendete RNN maximal sein kann. Dies bedeutet, dass das System bei einem Wert von z.B. 10 sich bis zu 10 Sensoreingaben, in dieser Arbeit 10 Eingabebilder, merkt und mit diesen die Steuerungsvorhersagen erstellt. Die Angabe der $sequence\_size$ bestimmt die maximale Länge der verwendeten Deque in den neuen RNN-Implementierung $KerasBaseModelRNN$. Die Deque ist eine standardmäßige Python Collection und wird als Zwischenspeicher für die letzten $n$-Bilder, $n = sequence\_size$, verwendet.

\section{Quellcodeanpassungen neuroracer-ai}
Um RNNs im \href{https://gitlab.com/NeuroRace/neuroracer-ai}{neuroracer-ai} Projekt nutzen zu können musste nur eine Spezialisierung der KerasBaseModel Klasse erstellt werden. Der Unterschied zwischen den bisherigen Implementierungen und dem neuen RNN-Ansatz ist, dass auf nicht nur einem aktuellen Bild die Steuerung bestimmt wird sondern, dass auf eine Sequenz von Bildern eine Aussage getroffen wird. Hierfür musste die KerasBaseModel Klasse erweitert werden durch die KerasBaseModelRNN Klasse. Diese verfügt über eine Deque welche die Bildersequenz darstellt.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{resources/code/neuroracer_ai_codeanpassungen}
	\caption{Klassendiagramm KerasBaseModelRNN}
	\label{img:class_dig_kerasBaseModelRNN}
\end{figure}

Eine Deque weißt genau die Eigenschaften auf welche auch von der Bildsequenz erwartet werden. Das erste Bild, dass der Deque hinzugefügt wird, wird auch als erstes wieder entfernt. Ein Bild welches älter ist als die gesetzte $sequence\_size$ wird aus der Deque entfernt und das aktuelle Bild wird der Sequenz hinzugefügt.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{resources/code/deque_beispiel}
	\caption{RNN Eingabesequenz Deque-Beispiel}
	\label{img:deque_example}
\end{figure}

Ein weiterer Grund warum die KerasBaseModel Klasse spezialisiert werden musst ist auch, das die Methode $predict(...)$ nun auf Basis der Deque die Vorhersagen des Netzes erstellen muss.
In der neuen Implementierung der Methode wird die Bildsequenz um neue Bilder erweitert und die Vorhersage mit Hilfe dieser erstellt. Der Ablauf dieses Vorgangs kann Abbildung \ref{img:deque_example} entnommen werden. Die Deque kann direkt nach der Umwandlung in ein Numpy-Array, so wie sie ist, für die Vorhersage an das Model übergeben werden.

Ebenso wurde die $build(...)$ Methode von BackendFactory so angepasst, dass diese nach Erhalt der neuen Backend-Konfiguration aus der jeweiligen YAML-Datei auch die neue Klasse KerasBaseModelRNN zurückgeben kann. Um den neuen Eintrag aus der YAML-Datei für die Factory bekannt zu machen, wurde die Enum-Klasse EKeys um den Wert $SEQUENCE\_SIZE$ erweitert.

\section{Quellcodeanpassungen neuroracer-ai-sl}
Damit im \href{https://gitlab.com/NeuroRace/neuroracer-ai-sl}{neuroracer-ai-sl} Projekt das Training auch für RNNs funktioniert musst hier ebenfalls eine Spezialisierung der bisherigen Implementierung erstellt werden. KerasModelSLRNN erbt von KerasModelSL und implementiert die Methode $train(...)$ neu. Der Unterschied ist, dass beim RNN-Ansatz Datengeneratoren verwendet werden müssen welche sequenzielle Daten zurückgeben.

Damit solche Datengeneratoren verwendet werden konnten, musste die bestehende KerasSequence Klasse ebenso spezialisiert werden. Die Klasse KerasSequenceRNN ist ein solcher Datengenerator für sequenzielle Daten. Dieser Generator gibt Daten-Batches mit folgenden Dimensionen zurück:
$$(batch\_size,\; sequence\_size,\; height,\; width,\; depth)$$

Die letzten Dimensionen hängen vom jeweils verwendeten SnapshotPrecessor ab. In dieser Arbeit wurde der SingleViewSnapshotProcessor verwendet. Wichtig zu erwähnen ist, das die $batch\_size$ bei der Verwendung des RNN-Ansatzes nicht zu groß gewählt werden sollte, da $batch\_size * sequence\_size$ viele Bilder vom Generator in den RAM geladen werden und nicht nur $batch\_size$ viele. Eine Übersicht der neuen Klassen kann Abbildung \ref{img:class_dig_kerasModelRNN_kerasSeqRNN} entnommen werden:

\begin{figure}[H]
	\centering
	\includegraphics[width=0.95\textwidth]{resources/code/neuroracer_ai_sl_codeanpassungen}
	\caption{Klassendiagramm KerasModelSLRNN und KerasSequenceRNN}
	\label{img:class_dig_kerasModelRNN_kerasSeqRNN}
\end{figure}

Die Klasse KerasArchitectures wurde um eine erste RNN-Netz-Methode erweitert um die Erstellung von RNN-Models zu ermöglichen. Wie auch bei neuroracer-ai wurde die $build(...)$ Methode der BackendFactorySL so angepasst, dass sie anhand des neuen Eintrags in den jeweiligen YAML-Konfigurationsdateien KerasModelSLRNN Instanzen zurückgeben kann.

\section{Recurrent Neural Network}
Das verwendete RNN wurde auf Basis von \cite{DBLP:journals/corr/abs-1801-06734} implementiert. Zuerst erfolgen vier verschiedene Convolutional-Layer. Hierbei werden die folgenden Operationen bei allen Convolutional-Layern in gleicher Reihenfolge durchgeführt:

\begin{enumerate}
\item Faltung mit der angegebenen Kernel-Größe und Tiefe
\item MaxPooling immer mit einem 2x2 Kernel
\item Batch-Normalisierung
\item Wie im BatchNorm-Paper \cite{DBLP:journals/corr/IoffeS15} angegeben, die Aktivierung, eben nach der Normalisierung
\end{enumerate}

Die genauen Parameter des implementierten RNNs mit LSTM-Zelle kann der folgenden Abbildung entnommen werden:

\begin{figure}[H]
	\centering
	\includegraphics[width=0.95\textwidth]{resources/implementation/lstm_net}
	\caption{RNN mit LSTM-Zelle}
	\label{img:rnn_w_lstm}
\end{figure}

Bei der Wahl der Kernel-Größe wie auch Tiefe wurde sich, wie bereits geschrieben, an \cite{DBLP:journals/corr/abs-1801-06734} orientiert. In dieser Ausarbeitung wurde empfohlen kleinere jedoch tiefe Kernel zu verwenden. Um keine zu hohe Parameteranzahl zu erhalten wurden jedoch die Werte für die Tiefen angepasst, also verkleinert. Auch wurden die Neuronenanzahl der Fully-Connected- und LSTM-Layer verkleinert. Es ist an dieser Stelle schon einmal vorwegzunehmen, dass für das Steuern des Roboters in der Simulation keine 1 Millionen Parameter notwendig waren, daher auch diese Anpassungen. Als Eingabe wurden Bilder mit der folgenden Abmessung verwendet:
$$(48, 96, 3)$$

Um diese Abmessung zu erhalten wurde beim Image-Processing zuerst 30\% des Bildes von oben abgeschnitten, dann eine Skalierung von 0,15 der Weite und der Höhe durchgeführt. Anschließend folgte eine Normalisierung des Bildes mit einem Normalisierungswert von 255.0.

Zur Überprüfung des RNN-Ansatzes, wurde auf Basis der gerade beschriebenen Netzarchitektur auch ein CNN implementiert. Dieses weist die gleiche Netzarchitektur auf, jedoch wurde die RNN-Zelle durch einen Fully-Connected-Layer ersetzt. Dies kann der Abbildung \ref{img:cnn_for_tests} entnommen werden.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.95\textwidth]{resources/implementation/cnn_net}
	\caption{CNN-Architektur zum Vergleich}
	\label{img:cnn_for_tests}
\end{figure}
