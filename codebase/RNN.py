from keras.utils import Sequence
import math
import cv2
import numpy as np
import os
import yaml


class MyKerasSequence(Sequence):
    def __init__(self, batch_size, sequence_size, main_dir):
        self.batch_size = batch_size
        self.sequence_size = sequence_size
        self.main_dir = main_dir
        self.data_list = []
        
        self.grayscale = False
        self.flip = False
        self.resize = True
        self.crop = True
        
        self._init()

    def _init(self):
        self._get_data_list()

    def __len__(self):
        if self.sequence_size == 0:
            length = math.ceil(len(self.data_list) / (self.batch_size))
        else:
            length = math.ceil(len(self.data_list) / (self.batch_size * self.sequence_size))
        return length
        
    def on_epoch_end(self):
        self.flip = not self.flip

    def __getitem__(self, idx):
        
        if self.sequence_size == 0:
            _, img_paths, y = zip(*self.data_list[idx * self.batch_size:(idx+1) * self.batch_size])
        else:
            _, img_paths, y = zip(*self.data_list[idx * self.batch_size * self.sequence_size:
                                                  (idx+1) * self.batch_size * self.sequence_size])
        imgs = []
        # self.flip = not self.flip

        # image processing
        for img_path in img_paths:
            img = cv2.imread(img_path)
            
            if self.grayscale:
                img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) # grayscaling
                
            img_shape = img.shape
            
            if self.crop:
                img = img[img_shape[0]//3:img_shape[0], 0:img_shape[1]]  # cropping
                
            if self.resize:
                img = cv2.resize(img, (0,0), fx=0.15, fy=0.15)  # resizing
            
            if self.flip:
                img = cv2.flip(img, 1)
            
            img = img / 255.0  # normalization
            
            if self.grayscale:
                img = np.expand_dims(img, axis=2) # if grayscaled
            
            imgs.append(img)
        
        if self.flip:
            y = [(steer*(-1)) for steer in y]
        
        
        reshaped_imgs = []
        reshaped_y = []
        
        if self.sequence_size == 0:
            reshaped_imgs = imgs
            reshaped_y = y
        else:
            imgs = np.asarray(imgs)
            # TODO: 56 < 60 bei Sequenz 30 und Batch 2, 56 in einer Sequenz
            # TODO: jedoch meckert Keras, wenn die Seqnece_length dimension unterschiedlich ist
            # TODO: Die Batch hat dann eine Shape von z.B. (8,1) heisst, Batch_size = 8
            # TODO: und das die Seqence_length unterschiedlich, werden einfach alle zusammengefasst
            # TODO: dargestellt, bei einer Batch_size von 1 und einer Sequence_length von 30
            # TODO: waere die Batch_shape (1, 2) die Shape von Batch[0] waere (2,) und die
            # TODO: Shape von Batch[0][0] waere z.B. (30, 80, 160, 3)
            if imgs.shape[0] < (self.batch_size * self.sequence_size):
                reshaped_imgs.append(np.array(imgs))
                reshaped_y.append(np.array([np.array([v]) for v in y]))
            else:
                for i in range(self.batch_size):
                    reshaped_imgs.append(np.array(imgs[i*self.sequence_size:(i+1)*self.sequence_size]))
                    reshaped_y.append(np.array([np.array([v]) for v in y[i*self.sequence_size:(i+1)*self.sequence_size]]))
        
        return np.asarray(reshaped_imgs), np.asarray(reshaped_y)

    def _get_data_list(self):
        yaml_list = []
        for main_dir in os.listdir(self.main_dir):
            current_dir = os.path.join(self.main_dir, main_dir)
            for sub_dir in os.listdir(current_dir):
                if ".yaml" in sub_dir:
                    yaml_path = os.path.join(current_dir, sub_dir)
                    with open(yaml_path, "r") as stream:
                        try:
                            yaml_content = yaml.load(stream)
                            yaml_list.append((yaml_path, yaml_content))
                        except yaml.YAMLError as exc:
                            print(exc)

        act_topic = "/engine_wrapper/output/default"
        data_list = []
        for yaml_path, yaml_file in yaml_list:
            for snapshot in yaml_file:
                timestamp = snapshot["timestamp"]
                steering =  snapshot["action_messages"][act_topic]["drive"]["steering_angle"]
                img_path_list = snapshot["camera_messages"]
                for img_path in img_path_list:
                    img_path = os.path.dirname(yaml_path) + img_path
                    if os.path.isfile(img_path):
                        data_list.append((timestamp, img_path, steering))
        self.data_list = data_list

